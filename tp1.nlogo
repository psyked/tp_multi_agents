turtles-own [
  ; agents in range
  flockmates
  ; steering forces
  VSep
  VAlign
  VCohere
  ; force to get items
  VItem
  ; items in range
  items
  ; 'team' object carriers
  Team
]

patches-own
[
  ObjectType
]

globals
[
  collisions
]

to setup
  clear-all
  create-turtles population
    [ set color white
      set size 1.5
      setxy random-xcor random-ycor
      set flockmates no-turtles
      set Team 0
    ]
    addItems
  reset-ticks
end

to go
  set collisions 0
  ask turtles [ flock ]
  ask turtles [find-item]
  repeat smooth-display [ ask turtles [ move ] display ]
  ask turtles [detectCollision]
  tick
end

to addItems
  if-else randomObjects
  [
    ask patches
    [
      if-else random-float 100 < nb-objects * 100 / count patches
      [
        let types (random nb-types-objects) + 1
        set pcolor item types base-colors
        set ObjectType types
      ]
      [
        set ObjectType 0
      ]
    ]
  ]
  [
    let i 10
    repeat 10
    [
      ask patch ((max-pxcor - min-pxcor / 2) + i) ((max-pycor - min-pycor / 2) + i)
      [
        set pcolor green
        set ObjectType 1
      ]

      ask patch ((max-pxcor - min-pxcor / 2) - i) ((max-pycor - min-pycor / 2) - i)
      [
        set pcolor green
        set ObjectType 1
      ]
      set i i - 1
    ]
  ]
end

to flock ;; turtle procedure
  find-flockmates

 ; the heading doesn't change if flockmates are not found
      set VSep angleToVect heading
      set VCohere angleToVect heading
      set VAlign angleToVect heading

  if any? flockmates
    [
      separate
      align
      cohere
    ]
end

;;; move using steering behavior

to move
  let steering addVectList (addVectList VSep VAlign) VCohere

  set steering addVectList steering VItem
  set steering multiplyScalVect (1 / 4) steering

  set steering addVectList(angleToVect heading) steering
  set heading vectToAngle (steering)
  let speed speedFromVect steering

  fd (speed / smooth-display)

end

to find-flockmates  ;; turtle procedure
  set flockmates other turtles in-radius vision
end

;;; Vector operators
to-report addVect [x1 y1 x2 y2]
  report (list (x1 + x2) (y1 + y2))
end

to-report addVectList [v1 v2]
  report (list (item 0 v1 + item 0 v2) (item 1 v1 + item 1 v2))
end

to-report angleToVect [angle]
  report (list cos(angle) sin(angle))
end

to-report vectToAngle [v]
  let angle 90 - atan item 0 v item 1 v
  if-else angle < 0
  [report 360 + angle]
  [report angle]
end

to-report multiplyScalVect [k v]
  report (list (k * item 0 v) (k * item 1 v))
end

to-report speedFromVect [v]
  report (sqrt (item 0 v * item 0 v + item 1 v * item 1 v))
end

;;; End Vector operators

;;; ALIGN

to align
  let angle angleToVect heading
  let cteam Team
  ask flockmates
  [
    ; check if in the same team
    if cteam = Team or nb-types-objects < 2
    [
      set angle addVectList angle angleToVect heading
    ]
  ]
  set Valign multiplyScalVect (align-force / count flockmates) angle
end

;;; separate

to separate
  let angle angleToVect heading
  let posAgentX xcor
  let posAgentY ycor
  ask flockmates
  [
    let avoidAngle angleToVect (towardsxy posAgentX posAgentY + 180)
    set avoidAngle multiplyScalVect (-1 / distancexy posAgentX posAgentY) avoidAngle
    set angle addVectList angle avoidAngle
  ]
  set angle multiplyScalVect (separation-force / count flockmates) angle
  set VSep angle
end

;;; cohere

to cohere
  let angle angleToVect heading
  let meanX dx
  let meanY dy
  if-else nb-types-objects > 1
  [ ; coher with all
    set meanX mean [dx] of flockmates
    set meanY mean [dy] of flockmates
  ]
  [ ; cohere with teammates only
    let cteam Team
    let nMates 0
    ask flockmates
    [
      ; check if in the same team
      if cteam = Team
      [
        set meanX meanX + dx
        set meanY meanY + dy
        set nMates nMates + 1
      ]
    ]
    if nMates > 0
    [
      set meanX meanX / nMates
      set meanY meanY / nMates
    ]
  ]
  set angle addVectList angle angleToVect towardsxy meanX meanY

  set angle multiplyScalVect (cohere-force / (vision - distancexy meanX meanY)) angle
  set VCohere angle
end

;;; adding collision detection

to detectCollision
  ; detect collisions to plot
  if any? other turtles-here
  [
      set collisions collisions + 1
  ]
  ; add collision with objects
  if [ObjectType] of patch-here != 0 and (Team = 0 or Team = [ObjectType] of patch-here)
  [
    set Team [ObjectType] of patch-here
    set color [pcolor] of patch-here
    ask patch-here
    [
      set ObjectType 0
      set pcolor black
    ]
  ]
end

;;; find closest item

to find-item
  if-else Team = 0
  [
    set items other patches in-radius (2 * vision)
    let minDist -1
    let closestItem 0
    let posX xcor
    let posY ycor
    ;let currentPatch patch-at xcor ycor
    ; find items through patches
    ask items
    [
      if [ObjectType] of self != 0
      [
        if minDist > distancexy posX posY or minDist < 0
        [
          ;if self != currentPatch
          ;[
          set closestItem self
          set minDist distancexy posX posY
          ;]
        ]
      ]
    ]
    ; set a vector to fetch that item
    if-else minDist > 0
    [
      set VItem multiplyScalVect fetch-force angleToVect (towards closestItem)
    ]
    [
      set VItem angleToVect heading
    ]
  ]
  [
    set VItem angleToVect heading
  ]
end
@#$#@#$#@
GRAPHICS-WINDOW
305
-3
727
440
35
35
5.803
1
10
1
1
1
0
1
1
1
-35
35
-35
35
1
1
1
ticks
30.0

BUTTON
41
57
118
90
NIL
setup
NIL
1
T
OBSERVER
NIL
NIL
NIL
NIL
1

BUTTON
124
57
205
90
NIL
go
T
1
T
OBSERVER
NIL
NIL
NIL
NIL
0

SLIDER
11
15
234
48
population
population
1.0
1000.0
97
1.0
1
NIL
HORIZONTAL

SLIDER
11
99
234
132
vision
vision
0.0
10.0
4
0.5
1
patches
HORIZONTAL

SLIDER
12
282
185
315
cohere-force
cohere-force
0.1
5
1
0.1
1
NIL
HORIZONTAL

SLIDER
13
238
185
271
align-force
align-force
.1
5
1
.1
1
NIL
HORIZONTAL

SLIDER
12
329
184
362
separation-force
separation-force
0.1
5
1
.1
1
NIL
HORIZONTAL

TEXTBOX
15
209
165
227
Pondération flocking :
14
0.0
1

PLOT
292
444
732
575
Number of collisions per tick
NIL
NIL
0.0
10.0
0.0
10.0
true
false
"" ""
PENS
"default" 1.0 0 -16777216 true "" "plot collisions / 2"

SLIDER
10
408
182
441
nb-objects
nb-objects
0
100
23
1
1
NIL
HORIZONTAL

SLIDER
15
162
185
195
smooth-display
smooth-display
1
10
6
1
1
NIL
HORIZONTAL

BUTTON
10
453
106
486
add objects
addItems
NIL
1
T
OBSERVER
NIL
NIL
NIL
NIL
1

SWITCH
115
453
255
486
randomObjects
randomObjects
0
1
-1000

TEXTBOX
17
378
167
396
Objects :
14
0.0
1

SLIDER
10
498
182
531
fetch-force
fetch-force
0.1
5
1
.1
1
NIL
HORIZONTAL

SLIDER
9
546
181
579
nb-types-objects
nb-types-objects
1
5
5
1
1
NIL
HORIZONTAL

@#$#@#$#@
# TP1 Multi agents

## Introduction
Ce document est la synthèse du travail réalisé pour
le premier devoir de systèmes multi-agents réalisé sous
NetLogo par Edouard François.

## Partie 1 : modifications flocking
Dans cette partie, nous apportons des modifications à l'algorithme de
base de flocking de NetLogo partagée dans les librairies modèles.
Pour cela, les modifications suivantes ont été effectuées :

 * L'ajout de trois sliders pour choisir l'intensité des différentes forces :
 * * séparation,
 * * cohésion
 * * et alignement.
 * L'intégration des pondérations dans les forces de flocking.
 * L'utilisation de la moyenne des positions des voisins pondérée par leur
nombre pour le calcul de cohésion et de séparation (à la place de ne
considéré que le voisin le plus proche).
 * La suppression des sliders minimum-separation et
max-align/cohere/separate-turn qui ne sont utilisés que dans la
version de flocking de NetLogo.

## Partie 2
Pour cette partie, on créé des patchs verts pour identifiers les objets et
on les fait spawn aléatoirement sur la carte ou dans le coté inférieur
gauche de l'écran.
On rajout aux agent une force suplémentaire de `fetch` (pondérable via slider)
s'ajoutant aux forces de flocking pour déterminer le steering behavior.
Ainsi si un item se trouve à proximité d'un agent, cet agent va la "ramasser" :
(il deviendra vert et le patch redeviendra noir) tout en continuant son
mouvement de flocking avec les autres agents.

## Partie 3
Pour que le flocking soit optimal, il faut que les trois forces aient la même
pondération, sinon on observe des déplacements ératiques : les agents
s'éloiognent les uns des autres et se dispersent sur tout l'écran ou il
se heurtent tous au même endroit, etc.
Il en va de même pour la force de `fetch` qui si elle diffère trop des
forces de flocking sera soit ignorée : les agents ne se déplaceront pas vers les
objets à proximité (force trop faible) ou les agents foncent vers l'objet
le plus proche et se heurtent les uns les autre (la force trop forte).

## Partie 4
Pour mesurer la performance du flocking, on affiche dans l'outil `Plot` de
NetLogo le nombre de fois par tick où un agent rentre dans un autre, on
retrouve cette valeur en demandant à chaque agent si un autre agent
se trouve sur le même patch, puis on divise la valeur trouvée par deux.

## Partie 5
Après avoir retrouvé un équilibre en ayant ramassé tous les objet
(cf. figure 50), le flocking devient instable si la force d'alignement
ou de cohésion devient le triple des autres (collisions détectées).
En revanche lorsque l'on augmente la force de séparation, on obtient
aussi un flocking instable mais notre benchmark ne le prends pas en compte
puisque se repoussant les uns les autres, les agents n'entrent pas en
collision et ceux même si la pondération de la force de cohésion est cinq
fois supérieure à celle des autres forces de flocking.

## Partie 6
On change ici les voisins pris en compte pour le flocking :
un agent a une force de séparation avec tous les agents dans son
champ de vision mais ne prends en compte que ses voisins de la même
équipe pour le calcul des forces d'alignement et de cohésion.
Des agents sont dans une même équipe s'ils ont ramassé le même
type d'objet (ou n'en ont pas encore ramassé).

De plus après avoir ramassé un objet, un agent ne pourra en prendre
un objet d'un autre type.

On ajoute aussi un slider permettant de choisir le nombre d'objets
différents à afficher.
NOTE: Si le nombre d'objets différents est inférieur à deux, les changement
de flocking et de ramassage d'objets ne s'appliquent pas (ainsi les
précédantes parties sont toujours testables si `nb-types-objects` vaut 1).

On voit ce que donne les équipes dans la la figures 60 :

* les blancs vont vers le bas
* le oranges vont vers le haut à gauche
* les marrons vont vers la droite
* Les rouges vont vers le bas à droite
* les jaunes ne se sont pas encore regroupés

## Partie 7
Il existe plusieurs différences entre le flocking de NetLogo et celui
implémenté ici :

* Celui de NetLogo ne prend pas en compte les forces vectorielles,
il ne fait que changer de direction pour chacune des 'forces' :
* * se tourne dans le sens opposé au voisin pour la séparation,
* * se tourne vers le voisin pour la cohésion,
* * se tourne dans le même que ses voisin pour l'alignement.
* Celui de NetLogo ne prend que *le voisin* le plus proche pour déterminer
sa force de séparation et d'alignement alors que le notre prend en compte
tous ses voisins et pondère leur importance en fonction de leur proximité
(un voisin proche aura une incidence plus importante qu'un voisin éloigné
dans le calcul des trois forces).
* Enfin notre algorithme permet de faire varier les pondérations des
différentes forces.

## Partie 8
Cf. apparition aléatoire au fil du temps d'objets disparaissant lors
du ramassage, on peut en rajouter via le bouton `add objects`.

## Partie 9
Cf. notes, plot et sliders dans l'interface pour visualiser facilement
les différentes forces et la qualité du flocking.
Ajout du slider `smooth-display` permettant de régler le rafraichissement
par tick de l'affichage.
Ajout de ce document dans l'onglet `info` du fichier NetLogo.
@#$#@#$#@
default
true
0
Polygon -7500403 true true 150 5 40 250 150 205 260 250

airplane
true
0
Polygon -7500403 true true 150 0 135 15 120 60 120 105 15 165 15 195 120 180 135 240 105 270 120 285 150 270 180 285 210 270 165 240 180 180 285 195 285 165 180 105 180 60 165 15

arrow
true
0
Polygon -7500403 true true 150 0 0 150 105 150 105 293 195 293 195 150 300 150

box
false
0
Polygon -7500403 true true 150 285 285 225 285 75 150 135
Polygon -7500403 true true 150 135 15 75 150 15 285 75
Polygon -7500403 true true 15 75 15 225 150 285 150 135
Line -16777216 false 150 285 150 135
Line -16777216 false 150 135 15 75
Line -16777216 false 150 135 285 75

bug
true
0
Circle -7500403 true true 96 182 108
Circle -7500403 true true 110 127 80
Circle -7500403 true true 110 75 80
Line -7500403 true 150 100 80 30
Line -7500403 true 150 100 220 30

butterfly
true
0
Polygon -7500403 true true 150 165 209 199 225 225 225 255 195 270 165 255 150 240
Polygon -7500403 true true 150 165 89 198 75 225 75 255 105 270 135 255 150 240
Polygon -7500403 true true 139 148 100 105 55 90 25 90 10 105 10 135 25 180 40 195 85 194 139 163
Polygon -7500403 true true 162 150 200 105 245 90 275 90 290 105 290 135 275 180 260 195 215 195 162 165
Polygon -16777216 true false 150 255 135 225 120 150 135 120 150 105 165 120 180 150 165 225
Circle -16777216 true false 135 90 30
Line -16777216 false 150 105 195 60
Line -16777216 false 150 105 105 60

car
false
0
Polygon -7500403 true true 300 180 279 164 261 144 240 135 226 132 213 106 203 84 185 63 159 50 135 50 75 60 0 150 0 165 0 225 300 225 300 180
Circle -16777216 true false 180 180 90
Circle -16777216 true false 30 180 90
Polygon -16777216 true false 162 80 132 78 134 135 209 135 194 105 189 96 180 89
Circle -7500403 true true 47 195 58
Circle -7500403 true true 195 195 58

circle
false
0
Circle -7500403 true true 0 0 300

circle 2
false
0
Circle -7500403 true true 0 0 300
Circle -16777216 true false 30 30 240

cow
false
0
Polygon -7500403 true true 200 193 197 249 179 249 177 196 166 187 140 189 93 191 78 179 72 211 49 209 48 181 37 149 25 120 25 89 45 72 103 84 179 75 198 76 252 64 272 81 293 103 285 121 255 121 242 118 224 167
Polygon -7500403 true true 73 210 86 251 62 249 48 208
Polygon -7500403 true true 25 114 16 195 9 204 23 213 25 200 39 123

cylinder
false
0
Circle -7500403 true true 0 0 300

dot
false
0
Circle -7500403 true true 90 90 120

face happy
false
0
Circle -7500403 true true 8 8 285
Circle -16777216 true false 60 75 60
Circle -16777216 true false 180 75 60
Polygon -16777216 true false 150 255 90 239 62 213 47 191 67 179 90 203 109 218 150 225 192 218 210 203 227 181 251 194 236 217 212 240

face neutral
false
0
Circle -7500403 true true 8 7 285
Circle -16777216 true false 60 75 60
Circle -16777216 true false 180 75 60
Rectangle -16777216 true false 60 195 240 225

face sad
false
0
Circle -7500403 true true 8 8 285
Circle -16777216 true false 60 75 60
Circle -16777216 true false 180 75 60
Polygon -16777216 true false 150 168 90 184 62 210 47 232 67 244 90 220 109 205 150 198 192 205 210 220 227 242 251 229 236 206 212 183

fish
false
0
Polygon -1 true false 44 131 21 87 15 86 0 120 15 150 0 180 13 214 20 212 45 166
Polygon -1 true false 135 195 119 235 95 218 76 210 46 204 60 165
Polygon -1 true false 75 45 83 77 71 103 86 114 166 78 135 60
Polygon -7500403 true true 30 136 151 77 226 81 280 119 292 146 292 160 287 170 270 195 195 210 151 212 30 166
Circle -16777216 true false 215 106 30

flag
false
0
Rectangle -7500403 true true 60 15 75 300
Polygon -7500403 true true 90 150 270 90 90 30
Line -7500403 true 75 135 90 135
Line -7500403 true 75 45 90 45

flower
false
0
Polygon -10899396 true false 135 120 165 165 180 210 180 240 150 300 165 300 195 240 195 195 165 135
Circle -7500403 true true 85 132 38
Circle -7500403 true true 130 147 38
Circle -7500403 true true 192 85 38
Circle -7500403 true true 85 40 38
Circle -7500403 true true 177 40 38
Circle -7500403 true true 177 132 38
Circle -7500403 true true 70 85 38
Circle -7500403 true true 130 25 38
Circle -7500403 true true 96 51 108
Circle -16777216 true false 113 68 74
Polygon -10899396 true false 189 233 219 188 249 173 279 188 234 218
Polygon -10899396 true false 180 255 150 210 105 210 75 240 135 240

house
false
0
Rectangle -7500403 true true 45 120 255 285
Rectangle -16777216 true false 120 210 180 285
Polygon -7500403 true true 15 120 150 15 285 120
Line -16777216 false 30 120 270 120

leaf
false
0
Polygon -7500403 true true 150 210 135 195 120 210 60 210 30 195 60 180 60 165 15 135 30 120 15 105 40 104 45 90 60 90 90 105 105 120 120 120 105 60 120 60 135 30 150 15 165 30 180 60 195 60 180 120 195 120 210 105 240 90 255 90 263 104 285 105 270 120 285 135 240 165 240 180 270 195 240 210 180 210 165 195
Polygon -7500403 true true 135 195 135 240 120 255 105 255 105 285 135 285 165 240 165 195

line
true
0
Line -7500403 true 150 0 150 300

line half
true
0
Line -7500403 true 150 0 150 150

pentagon
false
0
Polygon -7500403 true true 150 15 15 120 60 285 240 285 285 120

person
false
0
Circle -7500403 true true 110 5 80
Polygon -7500403 true true 105 90 120 195 90 285 105 300 135 300 150 225 165 300 195 300 210 285 180 195 195 90
Rectangle -7500403 true true 127 79 172 94
Polygon -7500403 true true 195 90 240 150 225 180 165 105
Polygon -7500403 true true 105 90 60 150 75 180 135 105

plant
false
0
Rectangle -7500403 true true 135 90 165 300
Polygon -7500403 true true 135 255 90 210 45 195 75 255 135 285
Polygon -7500403 true true 165 255 210 210 255 195 225 255 165 285
Polygon -7500403 true true 135 180 90 135 45 120 75 180 135 210
Polygon -7500403 true true 165 180 165 210 225 180 255 120 210 135
Polygon -7500403 true true 135 105 90 60 45 45 75 105 135 135
Polygon -7500403 true true 165 105 165 135 225 105 255 45 210 60
Polygon -7500403 true true 135 90 120 45 150 15 180 45 165 90

square
false
0
Rectangle -7500403 true true 30 30 270 270

square 2
false
0
Rectangle -7500403 true true 30 30 270 270
Rectangle -16777216 true false 60 60 240 240

star
false
0
Polygon -7500403 true true 151 1 185 108 298 108 207 175 242 282 151 216 59 282 94 175 3 108 116 108

target
false
0
Circle -7500403 true true 0 0 300
Circle -16777216 true false 30 30 240
Circle -7500403 true true 60 60 180
Circle -16777216 true false 90 90 120
Circle -7500403 true true 120 120 60

tree
false
0
Circle -7500403 true true 118 3 94
Rectangle -6459832 true false 120 195 180 300
Circle -7500403 true true 65 21 108
Circle -7500403 true true 116 41 127
Circle -7500403 true true 45 90 120
Circle -7500403 true true 104 74 152

triangle
false
0
Polygon -7500403 true true 150 30 15 255 285 255

triangle 2
false
0
Polygon -7500403 true true 150 30 15 255 285 255
Polygon -16777216 true false 151 99 225 223 75 224

truck
false
0
Rectangle -7500403 true true 4 45 195 187
Polygon -7500403 true true 296 193 296 150 259 134 244 104 208 104 207 194
Rectangle -1 true false 195 60 195 105
Polygon -16777216 true false 238 112 252 141 219 141 218 112
Circle -16777216 true false 234 174 42
Rectangle -7500403 true true 181 185 214 194
Circle -16777216 true false 144 174 42
Circle -16777216 true false 24 174 42
Circle -7500403 false true 24 174 42
Circle -7500403 false true 144 174 42
Circle -7500403 false true 234 174 42

turtle
true
0
Polygon -10899396 true false 215 204 240 233 246 254 228 266 215 252 193 210
Polygon -10899396 true false 195 90 225 75 245 75 260 89 269 108 261 124 240 105 225 105 210 105
Polygon -10899396 true false 105 90 75 75 55 75 40 89 31 108 39 124 60 105 75 105 90 105
Polygon -10899396 true false 132 85 134 64 107 51 108 17 150 2 192 18 192 52 169 65 172 87
Polygon -10899396 true false 85 204 60 233 54 254 72 266 85 252 107 210
Polygon -7500403 true true 119 75 179 75 209 101 224 135 220 225 175 261 128 261 81 224 74 135 88 99

wheel
false
0
Circle -7500403 true true 3 3 294
Circle -16777216 true false 30 30 240
Line -7500403 true 150 285 150 15
Line -7500403 true 15 150 285 150
Circle -7500403 true true 120 120 60
Line -7500403 true 216 40 79 269
Line -7500403 true 40 84 269 221
Line -7500403 true 40 216 269 79
Line -7500403 true 84 40 221 269

x
false
0
Polygon -7500403 true true 270 75 225 30 30 225 75 270
Polygon -7500403 true true 30 75 75 30 270 225 225 270

@#$#@#$#@
NetLogo 5.3.1
@#$#@#$#@
set population 200
setup
repeat 200 [ go ]
@#$#@#$#@
@#$#@#$#@
@#$#@#$#@
@#$#@#$#@
default
0.0
-0.2 0 0.0 1.0
0.0 1 1.0 0.0
0.2 0 0.0 1.0
link direction
true
0
Line -7500403 true 150 150 90 180
Line -7500403 true 150 150 210 180

@#$#@#$#@
0
@#$#@#$#@
